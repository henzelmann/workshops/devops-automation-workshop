# DevOps Automation Workshop

## Presentation
You can access the presentation shared during the workshop via [DevOps Automation Workshop 0428.pdf](DevOps Automation Workshop 0428.pdf)

## Lab 
To follow along with the labs step by step, use the [issues found in this project](https://gitlab.com/henzelmann/workshops/devops-automation-workshop/-/boards/4238558).
